import led
import time


modra = led.Led(18)
cervena = led.Led(23)
biela  = led.Led(24)

while 1:
    modra.turnOn()
    time.sleep(0.1)
    modra.turnOff()
    time.sleep(0.1)
    cervena.toggle()
    time.sleep(0.1)
    cervena.toggle()
